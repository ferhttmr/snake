#ifndef _SNAKE
#define _SNAKE

#include"Fruit.h"
#include"Location.h"


class Fruit;

class Snake
{
	public:
	void draw_snake();
	void draw_snake(int x, int y);
	bool snake_eats(Fruit *pfrt);
	bool is_snake_head;
	Location current_location;
	Location prev_location;
	bool is_snake_alive;
	
	Snake(int x, int y){
		current_location.x = x;
		current_location.y = y;
		is_snake_head = false;
	}
	
	~Snake()
	{
		
	}
	
	private:
	int direction;
	int prev_direction;
	void snake_travels();
	void snake_travels(int x, int y);
	void set_direction();
	
	
	

};

#endif
