#ifndef _GAME
#define _GAME

#include"Snake.h"
#include"Fruit.h"
#include<vector>

class Fruit;
class Snake;

class Game{
	
	public:
	void Update(void);
	void Draw(void);
	
	Fruit* frt;
	std::vector<Snake*> Snakes;

	int score;
	bool gameover;
	bool is_game_start_decision_make(void);
	void write_start_menu(void);
	char game_started;
	
	Game()
	{
		gameover = false;
		score = 0;
		game_started = 0;
	}
	
	private:
	bool Collusion_Dedection(std::vector<Snake*> &snk);
	void clear_console_cursor(void);
	void draw_score(void);
	
};

#endif
