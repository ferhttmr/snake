#include"Fruit.h"
#include"General_types.h"


using namespace std;

void Fruit::set_random_fruit_location(vector<Snake*> snk)
{
	Randomize();
	current_location.x = (rand() % (WIDTH - 2) + 1);
	current_location.y = (rand() % (HEIGHT) + 1);
	for(int index = 0; index < snk.size(); ++index)
	{
		if(snk[index]->current_location.x == current_location.x && snk[index]->current_location.y == current_location.y) 
		{
			set_random_fruit_location(snk);	
		}
	}
}

void Fruit::Draw_Fruit(vector<Snake*> snk)
{

	write_to_console(current_location.x, current_location.y, " ");
	
	set_random_fruit_location(snk);
	
	write_to_console(current_location.x, current_location.y, "*");

   	is_fruit_alive = true;
}
