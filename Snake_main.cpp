#include"Snake.h"
#include"Fruit.h"
#include"Game.h"
#include"General_types.h"
#include <windows.h>


using namespace std;


int main()
{
	Game gm;
	
	while(gm.game_started != 2)
	{
		gm.write_start_menu();
	
		while(!gm.is_game_start_decision_make());
		system("cls");
		
		if(gm.game_started == 1)
		{
			Randomize();
			Snake snk((rand() % (WIDTH - 5) + 2), (rand() % (HEIGHT - 4) + 2));
			snk.is_snake_head = true;
			gm.Snakes.push_back(&snk);
			Fruit frt_gm((rand() % (WIDTH - 4) + 2), (rand() % (HEIGHT - 4) + 2));
			gm.frt = &frt_gm;
			
			draw_frame();
			
			while(!(gm.gameover))
			{
				gm.Update();
				gm.Draw();
					
				Sleep(100);
			}
			
		}
	}
	system("cls");

	
		
	return 0;
}
