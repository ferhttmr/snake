#include"Game.h"
#include"Fruit.h"
#include"General_types.h"
#include <windows.h>
#include<iostream>
#include<conio.h>

using namespace std;



bool Game::Collusion_Dedection(vector<Snake*> &snk)
{
	for(int index = 1; index < snk.size(); ++index)
	{
		if(snk[0]->current_location.x == snk[index]->current_location.x && snk[0]->current_location.y == snk[index]->current_location.y) return true;
		
	}
	
	return false;
}



void Game::Update(void)
{
	if(Snakes[0]->snake_eats(frt))
	{
		Snakes.push_back(new Snake(Snakes.back()->prev_location.x,Snakes.back()->prev_location.y));
		++score;
	}
	
	if(Collusion_Dedection(Snakes))
	{
		Snakes.clear();
		for(int index = 0; index < Snakes.size(); ++index)
		{
			Snakes[index]->~Snake();
		}
		gameover = true;
		game_started = 0;
	
	}
}

void Game::Draw(void)
{
	if(gameover) system("cls");	
	else
	{
		for(int index = 0; index < Snakes.size(); ++index)
		{
			if(Snakes[index]->is_snake_head)Snakes[index]->draw_snake();	
			else Snakes[index]->draw_snake(Snakes[index - 1]->prev_location.x, Snakes[index - 1]->prev_location.y);
			
			if(!frt->is_fruit_alive)
			{
				frt->Draw_Fruit(Snakes);
			}
		}
		
		draw_score();
	}
	
	clear_console_cursor();
	
}



void Game::clear_console_cursor(void)
{
	HANDLE out = GetStdHandle(STD_OUTPUT_HANDLE);

    CONSOLE_CURSOR_INFO     cursorInfo;

    GetConsoleCursorInfo(out, &cursorInfo);
    cursorInfo.bVisible = false; // set the cursor visibility
    SetConsoleCursorInfo(out, &cursorInfo);
}

void Game::draw_score(void)
{   
	write_to_console(10, HEIGHT + 5, "Score: " + to_string(score));
}

void Game::write_start_menu()
{
	write_to_console(8, 10, "	 [NEW GAME]");
	write_to_console(8, 14, "[YES/y]           [NO/n]");
	if(score)
	{
		write_to_console(8, 16, "Game Over. Your score is: " + to_string(score));
	}
	clear_console_cursor();
	
}

bool Game::is_game_start_decision_make()
{
	char selection;
	bool decision_made = false;
	
	selection = getch();
	
	if(selection == 'y' || selection == 'Y')
	{
		game_started = 1;
		decision_made = true;
		gameover = false;
		score = 0;
	}
	else if(selection == 'n' || selection == 'N')
	{
		game_started = 2;
		decision_made = true;
	}
	
	return decision_made;
}
