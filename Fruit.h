#ifndef _FRUIT
#define _FRUIT

#include"Location.h"
#include"Snake.h"
#include<vector>


class Snake;

class Fruit
{
	public:
	void set_random_fruit_location(std::vector<Snake*>);
	void Draw_Fruit(std::vector<Snake*> snk);
	bool is_fruit_alive;
	Location current_location;
	
	Fruit(int x, int y) {
		current_location.x = x; 
		current_location.y = y;
		is_fruit_alive = false;
	}
	

};

#endif
