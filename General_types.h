#ifndef GENERAL_TYPES
#define GENERAL_TYPES
#include<string>

#define HEIGHT 20
#define WIDTH  40

enum DIRECTION_TYPE
{
	STOP = 0,
	LEFT,
	RIGHT,
	UP,
	DOWN,
	
	TotalDirections
};


extern void write_to_console(int x, int y, std::string str);

extern void draw_frame(void);

extern void Randomize(void);
#endif
