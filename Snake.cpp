#include"Snake.h"
#include"General_types.h"
#include <conio.h>


using namespace std;

void Snake::snake_travels()
{
	
	prev_location.x = current_location.x;
	prev_location.y = current_location.y;
	
	if(is_snake_head)
	{
		
		switch(direction)
		{
		
			case UP:{ //Up
				if(current_location.y > 1) current_location.y = current_location.y - 1;
				else current_location.y = HEIGHT + 1;
				break;
			}
			case LEFT:{ //Left
				if(current_location.x > 1) current_location.x = current_location.x - 1;
				else current_location.x = WIDTH - 1;
				break;
			}
			case DOWN:{ //Down
				if(current_location.y < HEIGHT + 1) current_location.y = current_location.y + 1;
				else current_location.y = 1;
				break;
			}
			case RIGHT:{ //Right
				
				if(current_location.x < (WIDTH - 1))current_location.x = current_location.x + 1;
				else current_location.x = 1;
				break;
			}
			
			default:{
				if(current_location.x < (WIDTH - 1))current_location.x = current_location.x + 1;
				else current_location.x = 1;
				break;
			}
			
		}
		
	}
	
}

void Snake::set_direction()
{
	if(_kbhit())
	{
		switch(getch())
		{
			case 'w': {
				if(prev_direction != DOWN) direction = UP;
				break;
			}
			case 'a':
			{
				if(prev_direction != RIGHT) direction = LEFT; break;	
			 } 
			case 's': {
				if(prev_direction != UP) direction = DOWN; break;
			}
			case 'd': {
				if(prev_direction != LEFT) direction = RIGHT; break;
			}
		}
	}
	prev_direction = direction;
}

void Snake::draw_snake()
{
	write_to_console(current_location.x, current_location.y, " ");

	set_direction();
	snake_travels();
	
	
	write_to_console(current_location.x, current_location.y, "O");


}

bool Snake::snake_eats(Fruit *pfrt)
{
	if(current_location.x == pfrt->current_location.x && current_location.y == pfrt->current_location.y)
	{
		pfrt->is_fruit_alive = false;
		return true;
	}
	
	return false;
}

void Snake::snake_travels(int x, int y)
{
	prev_location.x = current_location.x;
	prev_location.y = current_location.y;
	
	current_location.x = x;
	current_location.y = y;
}

void Snake::draw_snake(int x, int y)
{
	write_to_console(current_location.x, current_location.y, " ");
	snake_travels(x, y);
	write_to_console(current_location.x, current_location.y, "#");
}

